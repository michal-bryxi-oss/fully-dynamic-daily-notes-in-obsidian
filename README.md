# Fully dynamic daily notes in Obsidian

This is an [Obsidian](https://obsidian.md/) vault pre-configured according to dev.to article [Fully dynamic daily notes in Obsidian](https://dev.to/michalbryxi/fully-dynamic-daily-notes-in-obsidian-2p42).

- If you're familiar with [git](https://git-scm.com/) you can just clone this repository and open it as new vault in Obsidian.
- If not, you can download this repository via `Code (menu item) -> Repository -> Code (button) -> Download source code -> zip`, extract the zip archive and then open the folder as new vault in Obsidian.